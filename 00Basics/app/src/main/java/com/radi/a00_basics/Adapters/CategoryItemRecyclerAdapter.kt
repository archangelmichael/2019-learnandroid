package com.radi.a00_basics.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.radi.a00_basics.Models.CategoryItem
import com.radi.a00_basics.R

class CategoryItemRecyclerAdapter(val context: Context,
                                  private val items: List<CategoryItem>,
                                  val itemClick: (CategoryItem) -> Unit) :
    RecyclerView.Adapter<CategoryItemRecyclerAdapter.CategoryItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryItemViewHolder {
        val categoryItemView = LayoutInflater.from(context).inflate(R.layout.category_item_list_item, parent, false)
        return CategoryItemViewHolder(categoryItemView, itemClick)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: CategoryItemViewHolder, position: Int) {
        holder.bindItem(getItemAt(position), context)
    }

    private fun getItemAt(position: Int): CategoryItem {
        return items[position]
    }

    inner class CategoryItemViewHolder(itemView: View,
                                       val itemClick: (CategoryItem) -> Unit) : RecyclerView.ViewHolder(itemView) {
        private val headerTextView: TextView = itemView.findViewById(R.id.titleTextView)
        private val backgroundImageView: ImageView = itemView.findViewById(R.id.backgroundImageView)

        fun bindItem(item: CategoryItem, context: Context) {
            headerTextView.text = item.toString()
            val resourceID = context.resources.getIdentifier(item.imageName, "drawable", context.packageName)
            backgroundImageView.setImageResource(resourceID)
            itemView.setOnClickListener { itemClick(item) }
        }
    }
}

