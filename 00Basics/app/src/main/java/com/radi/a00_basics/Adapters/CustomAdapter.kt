package com.radi.a00_basics.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.radi.a00_basics.Models.Category
import com.radi.a00_basics.R

class CustomAdapter(val context: Context, private val items: List<Category>): BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val categoryView: View
        val holder: ViewHolder

        if (convertView == null) {
            categoryView = LayoutInflater.from(context).inflate(R.layout.simple_list_item, null)
            val headerTextView: TextView = categoryView.findViewById(R.id.titleTextView)
            val backgroundImageView: ImageView = categoryView.findViewById(R.id.backgroundImageView)
            holder = ViewHolder(headerTextView, backgroundImageView)
            categoryView.tag = holder
        }
        else {
            holder = convertView.tag as ViewHolder
            categoryView = convertView
        }

        val item: Category? = getItem(position) as? Category
        if (item != null) {
            holder.headerTextView.text = item.toString()
            val resourceID = context.resources.getIdentifier(item.imageName, "drawable", context.packageName)
            holder.backgroundImageView.setImageResource(resourceID)
        }

        return categoryView
    }

    override fun getItem(position: Int): Any {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return  0
    }

    override fun getCount(): Int {
        return  items.count()
    }

    private class ViewHolder(val headerTextView: TextView, val backgroundImageView: ImageView)
}