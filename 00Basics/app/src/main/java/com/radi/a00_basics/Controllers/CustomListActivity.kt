package com.radi.a00_basics.Controllers

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.radi.a00_basics.Adapters.CustomAdapter
import com.radi.a00_basics.Models.Category
import com.radi.a00_basics.R
import com.radi.a00_basics.Services.DataService
import com.radi.a00_basics.Utils.Constants
import kotlinx.android.synthetic.main.activity_simple_list.*
import java.io.Serializable

class CustomListActivity : AppCompatActivity() {

    private lateinit var adapter: CustomAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_list)

        adapter = CustomAdapter(this, DataService.allCategories)
        simpleListView.adapter = adapter
        simpleListView.setOnItemClickListener { parent, _, position, _ ->
            val item = parent.getItemAtPosition(position) as? Category
            if (item != null) {
                goToCustomRecyclerView(item)
            }
        }
    }

    fun goToCustomRecyclerView(category: Category) {
        val intent = Intent(this, CustomRecyclerActivity::class.java)
        intent.putExtra(Constants.KEY_SELECTED_CATEGORY, category as Serializable)
        startActivity(intent)
    }
}
