package com.radi.a00_basics.Controllers

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.radi.a00_basics.Adapters.CategoryItemRecyclerAdapter
import com.radi.a00_basics.Extensions.Context.toast
import com.radi.a00_basics.Models.Category
import com.radi.a00_basics.Models.CategoryItem
import com.radi.a00_basics.R
import com.radi.a00_basics.Services.DataService
import com.radi.a00_basics.Utils.Constants
import kotlinx.android.synthetic.main.activity_custom_recycler.*

class CustomRecyclerActivity : AppCompatActivity() {

    private lateinit var selectedCategory: Category
    private lateinit var items: List<CategoryItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_recycler)
        selectedCategory = intent.getSerializableExtra(Constants.KEY_SELECTED_CATEGORY) as Category
        items = DataService.getItemsFor(selectedCategory)

        resources.configuration.orientation

        items_recycler_view.adapter = CategoryItemRecyclerAdapter(this, items) {
            this.toast("Clicked on $it")
        }

        items_recycler_view.hasFixedSize()

        val isInLandscape = resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
        val isOnTablet = resources.configuration.screenWidthDp > 720
        var itemsPerRow = 2
        if(!isOnTablet) {
            itemsPerRow = if (isInLandscape) 3 else 2
        } else {
            itemsPerRow = if (isInLandscape) 4 else 3
        }

        items_recycler_view.layoutManager = GridLayoutManager(this, itemsPerRow)


        // Initialize with linear layout
        /*
        items_recycler_view.apply {
            adapter = CategoryItemRecyclerAdapter(context, items) {
                this.context.toast("Clicked on $it")
            }

            hasFixedSize()
            layoutManager = LinearLayoutManager(context)
        }
         */
    }

    override fun onResume() {
        super.onResume()
        this.toast("Clicked on $selectedCategory with ${items.count()} Items")
    }
}
