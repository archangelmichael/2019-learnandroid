package com.radi.a00_basics.Controllers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.radi.a00_basics.R

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}
