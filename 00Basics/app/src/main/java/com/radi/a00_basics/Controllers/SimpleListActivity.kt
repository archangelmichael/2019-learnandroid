package com.radi.a00_basics.Controllers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import com.radi.a00_basics.Models.Category
import com.radi.a00_basics.R
import com.radi.a00_basics.Services.DataService
import kotlinx.android.synthetic.main.activity_simple_list.*

class SimpleListActivity : AppCompatActivity() {

    private lateinit var adapter: ArrayAdapter<Category>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_list)

        adapter = ArrayAdapter(this,
            android.R.layout.simple_list_item_1,
            DataService.allCategories)
        simpleListView.adapter = adapter
    }
}
