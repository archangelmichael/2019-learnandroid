package com.radi.a00_basics.Models

import java.io.Serializable

class Category(private val title: String, val imageName: String) : Serializable {
    override fun toString(): String {
        return this.title.toUpperCase()
    }
}