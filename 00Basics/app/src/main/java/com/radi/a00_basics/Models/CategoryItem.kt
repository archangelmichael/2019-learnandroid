package com.radi.a00_basics.Models

class CategoryItem(private val name: String, val imageName: String) {
    override fun toString(): String {
        return this.name.toUpperCase()
    }
}