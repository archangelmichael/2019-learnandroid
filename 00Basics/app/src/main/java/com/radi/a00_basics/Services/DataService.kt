package com.radi.a00_basics.Services

import com.radi.a00_basics.Models.Category
import com.radi.a00_basics.Models.CategoryItem

object DataService {
    val allCategories = listOf(
        Category("hats", "hatimage"),
        Category("hoodies", "hoodieimage"),
        Category("shirts", "shirtimage"),
        Category("hats", "hatimage"),
        Category("hoodies", "hoodieimage"),
        Category("shirts", "shirtimage"),
        Category("hats", "hatimage"),
        Category("hoodies", "hoodieimage"),
        Category("shirts", "shirtimage"),
        Category("hats", "hatimage"),
        Category("hoodies", "hoodieimage"),
        Category("shirts", "shirtimage"),
        Category("hats", "hatimage"),
        Category("hoodies", "hoodieimage"),
        Category("shirts", "shirtimage")
    )

    private val hats = listOf(
        CategoryItem("hat 1", "hat1"),
        CategoryItem("hat 2", "hat2"),
        CategoryItem("hat 3", "hat3"),
        CategoryItem("hat 4", "hat4"),
        CategoryItem("hat 1", "hat1"),
        CategoryItem("hat 2", "hat2"),
        CategoryItem("hat 3", "hat3"),
        CategoryItem("hat 4", "hat4"),
        CategoryItem("hat 1", "hat1"),
        CategoryItem("hat 2", "hat2"),
        CategoryItem("hat 3", "hat3"),
        CategoryItem("hat 4", "hat4")
    )

    private val hoodies = listOf(
        CategoryItem("hoodie 1", "hoodie1"),
        CategoryItem("hoodie 2", "hoodie2"),
        CategoryItem("hoodie 3", "hoodie3"),
        CategoryItem("hoodie 4", "hoodie4"),
        CategoryItem("hoodie 1", "hoodie1"),
        CategoryItem("hoodie 2", "hoodie2"),
        CategoryItem("hoodie 3", "hoodie3"),
        CategoryItem("hoodie 4", "hoodie4"),
        CategoryItem("hoodie 1", "hoodie1"),
        CategoryItem("hoodie 2", "hoodie2"),
        CategoryItem("hoodie 3", "hoodie3"),
        CategoryItem("hoodie 4", "hoodie4"),
        CategoryItem("hoodie 1", "hoodie1"),
        CategoryItem("hoodie 2", "hoodie2"),
        CategoryItem("hoodie 3", "hoodie3"),
        CategoryItem("hoodie 4", "hoodie4")
    )

    private val shirts = listOf(
        CategoryItem("shirt 1", "shirt1"),
        CategoryItem("shirt 2", "shirt2"),
        CategoryItem("shirt 3", "shirt3"),
        CategoryItem("shirt 4", "shirt4"),
        CategoryItem("shirt 1", "shirt1"),
        CategoryItem("shirt 2", "shirt2"),
        CategoryItem("shirt 3", "shirt3"),
        CategoryItem("shirt 4", "shirt4"),
        CategoryItem("shirt 1", "shirt1"),
        CategoryItem("shirt 2", "shirt2"),
        CategoryItem("shirt 3", "shirt3"),
        CategoryItem("shirt 4", "shirt4"),
        CategoryItem("shirt 1", "shirt1"),
        CategoryItem("shirt 2", "shirt2"),
        CategoryItem("shirt 3", "shirt3"),
        CategoryItem("shirt 4", "shirt4")
    )

    fun getItemsFor(category: Category): List<CategoryItem> {
        return when (category.toString()) {
            "HATS"-> hats
            "HOODIES" -> hoodies
            "SHIRTS" -> shirts
            else -> hats
        }
    }
}