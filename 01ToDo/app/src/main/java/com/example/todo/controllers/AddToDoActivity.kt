package com.example.todo.controllers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.todo.R
import com.example.todo.services.DataService
import kotlinx.android.synthetic.main.activity_add_to_do.*

class AddToDoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_to_do)
        addButton.setOnClickListener {
            val todoTitle = titleEditText.text.toString()
            val todoIsImportant = importantCheckBox.isChecked
            DataService.addTodo(todoTitle, todoIsImportant) {
                if (it) {
                    finish()
                } else {
                    Toast.makeText(this, "Could not create a ToDo", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}
