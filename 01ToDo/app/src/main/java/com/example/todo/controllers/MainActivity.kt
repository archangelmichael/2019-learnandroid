package com.example.todo.controllers

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import com.example.todo.R
import com.example.todo.services.DataService

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        addButton.setOnClickListener {
            val intent = Intent(this, AddToDoActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        updateListView()
    }

    private fun updateListView() {
        val todos = DataService.getTodoItems()

        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, todos)
        todosListView.adapter = adapter
        todosListView.setOnItemClickListener { parent, view, position, id ->
            val todo = todos[position]
            if (todo != null) {
                DataService.completeTodo(todo.id)
            }

            updateListView()
        }

        val hasItems = todos.count() > 0
        todosListView.isVisible = hasItems
        noTodosTextView.isVisible = !hasItems
        if (hasItems) {
            Log.d("todo", "There are ${todos.count()} todos")
        }
    }
}
