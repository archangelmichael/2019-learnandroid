package com.example.todo.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ToDoItem(@PrimaryKey var id: String = "",
                    private var title: String = "Empty",
                    private var important: Boolean = false,
                    var completed: Boolean = false): RealmObject() {

    override fun toString(): String {
        return title
    }
}