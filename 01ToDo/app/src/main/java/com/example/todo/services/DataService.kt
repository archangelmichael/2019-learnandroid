package com.example.todo.services

import com.example.todo.models.ToDoItem
import io.realm.Realm
import io.realm.RealmResults
import io.realm.kotlin.where
import java.util.*

object DataService {

    private val realm: Realm by lazy {
        Realm.getDefaultInstance()
    }

    fun getTodoItems(): RealmResults<ToDoItem> {
        return realm.where<ToDoItem>().equalTo("completed", false).findAll()
    }

    fun completeTodo(itemId: String) {
        realm.beginTransaction()
        val item = realm.where<ToDoItem>().equalTo("id", itemId).findFirst()
        item?.completed = true
        realm.commitTransaction()
    }

    fun addTodo(title: String,
                important: Boolean,
                callback: ((Boolean)->Unit)? = null) {
        if (title.isEmpty()) {
            callback?.invoke(false)
            return
        }

        val toDoItem = ToDoItem(UUID.randomUUID().toString(), title, important)
        addTodoItem(toDoItem)
        callback?.invoke(true)
    }

    private fun addTodoItem(item: ToDoItem) {
        realm.beginTransaction()
        realm.copyToRealm(item)
        realm.commitTransaction()
    }
}