package com.radi.findrepo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.radi.findrepo.models.SearchResult
import com.radi.findrepo.services.GitRetriever
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchResultsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_results)

        val searchText = intent.getStringExtra("searchText")
        println(searchText)

        val searchCallback = object : Callback<SearchResult> {
            override fun onResponse(call: Call<SearchResult>, response: Response<SearchResult>) {

                val searchResult = response.body()
                if (searchResult != null) {
                    for (repo in searchResult.items) {
                        println(repo.full_name)
                    }
                }
            }

            override fun onFailure(call: Call<SearchResult>, t: Throwable) {
                println("ERROR")
            }
        }

        val retriever = GitRetriever()
        retriever.searchRepo(searchCallback, searchText)
    }
}
