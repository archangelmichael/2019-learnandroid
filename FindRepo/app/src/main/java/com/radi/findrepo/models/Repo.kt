package com.radi.findrepo.models

class Repo (val full_name: String, val owner: RepoOwner, val html_url: String)