package com.radi.findrepo.services
import com.radi.findrepo.models.Repo
import com.radi.findrepo.models.SearchResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GitAPIService {

    // Has to call https://api.github.com/search/repositories?q=test
    @GET("search/repositories?")
    fun searchRepos(@Query("q") searchParameter:String) : Call<SearchResult>
}