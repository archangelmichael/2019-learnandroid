package com.radi.findrepo.services

import com.radi.findrepo.models.SearchResult
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class GitRetriever {
    private val service: GitAPIService

    init {
        val retrofit = Retrofit.Builder().baseUrl("https://api.github.com/").addConverterFactory(GsonConverterFactory.create()).build()
        service = retrofit.create(GitAPIService::class.java)
    }

    fun searchRepo(callback: Callback<SearchResult>, searchText: String) {
        var search: String = searchText
        if (search.isEmpty()) {
            search = "random"
        }

        val serviceCall = service.searchRepos(search)
        serviceCall.enqueue(callback)
    }
}